# Using Yolo to detect illustration in a corpus of early modern printed books

This Repo contains a keras based python implementation of the object detection framework YOLOv3. The source has originally has been forked from xx refined, adapted to and streamlined for the task at hand 

We include a model (model_data/illu\_weights\_final) that has been trained to detect illustrations in a corpus of early modern books. The data on which the model was trained is published in Zenoodo ().

Additionally we provide a script to retrain the model and a Jupyter notebook for batch prediction (ToDo make this part of the detection script)

The project is in detail described in "Using deep learning to detect Illustrations in a corpus of early modern printed books (forthcoming)"

## Installing:

The illustration detection currently only works with an outdated version of tensorflow (1.14.0). It is hence recommended to install the dependencies in a virtual enviroment:

* _pip install venv_ to create your environment.
* _source venv/bin/activate_ to enter the virtual environment.
* _pip install -r requirements.txt_ to install the requirements in the current environment.



## Training
* Step 1: Define training input
* Step 2 (otional): define anchorboxes 
* Step 3: train or retrain the model

### Define training input:

Trainingdata stored in *train.txt* has the format

*image\_1_path object\_1 \[object\_2 ...object\_n\]*

Each object is defined by a bounding box and a class number where boxes are specified by 

*a,b,c,d, class nr.*

Class numbers are mapped to class names in the file *classes.txt* in the format

*class_1<br>
class_2<br>
...*

Class number zero correspond to the first, class number 1 to the second item in the list, etc. 

### Define anchorboxes

Yolo3 uses 9 so called anchor boxes of predefined sizes and aspect ratios. Instead of predicting bounding boxes for detected objects from scratch, Yolo3 predicts off-sets with respect to these predefined anchor boxes. The file *default\_yolo\_anchors.txt* specifies a set of 9 boxes covering the different possible scales and aspect ratios.

A specific dataset will, however, have specific distribution of box sizes and aspect ratios and training based on a set of anchor boxes reflecting this specific distribution will, as a rule, yield better results. If the file *my\_yolo\_anchors.txt* is provided, training will uses the specific anchor boxes provided there.


ToDo: dependence on input size 

(N.B.: Whereas in Yolo 2 width, height of the anchors were specified with to the final feature map, in Yolo 3 they are specified with respect to the input size of the image, i.e. am an anchor box  16,30 for instance means a box of width 16 height 30 in the image scaled to the input size of the model)


### Train a model

To train a model call

python train.py

This will 









 



